from pdflib_py import *


class PDFlib(object):

    def __init__(self):
        self.p = PDF_new()
        PDF_set_parameter(self.p, "objorient", "true")

    # it is recommended not to use __del__ as it is not guaranteed
    # when this will be executed (see Python Esential Reference Page 94).
    # so we also implement a delete method and invalidate self.p
    # whenever this will be called.
    def __del__(self):
        if (self.p):
            PDF_delete(self.p)

    def delete(self):
        if (self.p):
            PDF_delete(self.p)
        self.p = 0

    def activate_item(self, id):
        PDF_activate_item(self.p, id)

    def add_bookmark(self, text, parent, open):
        return PDF_add_bookmark(self.p, text, parent, open)

    def add_launchlink(self, llx, lly, urx, ury, filename):
        PDF_add_launchlink(self.p, llx, lly, urx, ury, filename)

    def add_locallink(self, llx, lly, urx, ury, page, optlist):
        PDF_add_locallink(self.p, llx, lly, urx, ury, page, optlist)

    def add_nameddest(self, name, optlist):
        PDF_add_nameddest(self.p, name, optlist)

    def add_note(self, llx, lly, urx, ury, contents, title, icon, open):
        PDF_add_note(self.p, llx, lly, urx, ury, contents, title, icon, open)

    def add_path_point(self, path, x, y, type, optlist):
        return PDF_add_path_point(self.p, path, x, y, type, optlist)

    def add_pdflink(self, llx, lly, urx, ury, filename, page, optlist):
        PDF_add_pdflink(self.p, llx, lly, urx, ury, filename, page, optlist)

    def add_portfolio_file(self, folder, filename, optlist):
        return PDF_add_portfolio_file(self.p, folder, filename, optlist)

    def add_portfolio_folder(self, parent, foldername, optlist):
        return PDF_add_portfolio_folder(self.p, parent, foldername, optlist)

    def add_table_cell(self, table, column, row, text, optlist):
        return PDF_add_table_cell(self.p, table, column, row, text, optlist)

    def add_textflow(self, textflow, text, optlist):
        return PDF_add_textflow(self.p, textflow, text, optlist)

    def add_thumbnail(self, image):
        PDF_add_thumbnail(self.p, image)

    def add_weblink(self, llx, lly, urx, ury, url):
        PDF_add_weblink(self.p, llx, lly, urx, ury, url)

    def align(self, dx, dy):
        PDF_align(self.p, dx, dy)

    def arc(self, x, y, r, alpha, beta):
        PDF_arc(self.p, x, y, r, alpha, beta)

    def arcn(self, x, y, r, alpha, beta):
        PDF_arcn(self.p, x, y, r, alpha, beta)

    def attach_file(self, llx, lly, urx, ury, filename, description, author, mimetype, icon):
        PDF_attach_file(self.p, llx, lly, urx, ury, filename, description, author, mimetype, icon)

    def begin_document(self, filename, optlist):
        return PDF_begin_document(self.p, filename, optlist)

    def begin_dpart(self, optlist):
        PDF_begin_dpart(self.p, optlist)

    def begin_font(self, fontname, a, b, c, d, e, f, optlist):
        PDF_begin_font(self.p, fontname, a, b, c, d, e, f, optlist)

    def begin_glyph(self, glyphname, wx, llx, lly, urx, ury):
        PDF_begin_glyph(self.p, glyphname, wx, llx, lly, urx, ury)

    def begin_glyph_ext(self, uv, optlist):
        PDF_begin_glyph_ext(self.p, uv, optlist)

    def begin_item(self, tagname, optlist):
        return PDF_begin_item(self.p, tagname, optlist)

    def begin_layer(self, layer):
        PDF_begin_layer(self.p, layer)

    def begin_mc(self, tagname, optlist):
        PDF_begin_mc(self.p, tagname, optlist)

    def begin_page(self, width, height):
        PDF_begin_page(self.p, width, height)

    def begin_page_ext(self, width, height, optlist):
        PDF_begin_page_ext(self.p, width, height, optlist)

    def begin_pattern(self, width, height, xstep, ystep, painttype):
        return PDF_begin_pattern(self.p, width, height, xstep, ystep, painttype)

    def begin_pattern_ext(self, width, height, optlist):
        return PDF_begin_pattern_ext(self.p, width, height, optlist)

    def begin_template(self, width, height):
        return PDF_begin_template(self.p, width, height)

    def begin_template_ext(self, width, height, optlist):
        return PDF_begin_template_ext(self.p, width, height, optlist)

    def circle(self, x, y, r):
        PDF_circle(self.p, x, y, r)

    def circular_arc(self, x1, y1, x2, y2):
        PDF_circular_arc(self.p, x1, y1, x2, y2)

    def clip(self):
        PDF_clip(self.p)

    def close(self):
        PDF_close(self.p)

    def close_font(self, font):
        PDF_close_font(self.p, font)

    def close_graphics(self, graphics):
        PDF_close_graphics(self.p, graphics)

    def close_image(self, image):
        PDF_close_image(self.p, image)

    def close_pdi(self, doc):
        PDF_close_pdi(self.p, doc)

    def close_pdi_document(self, doc):
        PDF_close_pdi_document(self.p, doc)

    def close_pdi_page(self, page):
        PDF_close_pdi_page(self.p, page)

    def closepath(self):
        PDF_closepath(self.p)

    def closepath_fill_stroke(self):
        PDF_closepath_fill_stroke(self.p)

    def closepath_stroke(self):
        PDF_closepath_stroke(self.p)

    def concat(self, a, b, c, d, e, f):
        PDF_concat(self.p, a, b, c, d, e, f)

    def continue_text(self, text):
        PDF_continue_text(self.p, text)

    def convert_to_unicode(self, inputformat, inputstring, optlist):
        return PDF_convert_to_unicode(self.p, inputformat, inputstring, optlist)

    def create_3dview(self, username, optlist):
        return PDF_create_3dview(self.p, username, optlist)

    def create_action(self, type, optlist):
        return PDF_create_action(self.p, type, optlist)

    def create_annotation(self, llx, lly, urx, ury, type, optlist):
        PDF_create_annotation(self.p, llx, lly, urx, ury, type, optlist)

    def create_devicen(self, optlist):
        return PDF_create_devicen(self.p, optlist)

    def create_bookmark(self, text, optlist):
        return PDF_create_bookmark(self.p, text, optlist)

    def create_field(self, llx, lly, urx, ury, name, type, optlist):
        PDF_create_field(self.p, llx, lly, urx, ury, name, type, optlist)

    def create_fieldgroup(self, name, optlist):
        PDF_create_fieldgroup(self.p, name, optlist)

    def create_gstate(self, optlist):
        return PDF_create_gstate(self.p, optlist)

    def create_pvf(self, filename, data, optlist):
        PDF_create_pvf(self.p, filename, data, optlist)

    def create_textflow(self, text, optlist):
        return PDF_create_textflow(self.p, text, optlist)

    def curveto(self, x1, y1, x2, y2, x3, y3):
        PDF_curveto(self.p, x1, y1, x2, y2, x3, y3)

    def define_layer(self, name, optlist):
        return PDF_define_layer(self.p, name, optlist)

    def delete_path(self, path):
        PDF_delete_path(self.p, path)

    def delete_pvf(self, filename):
        return PDF_delete_pvf(self.p, filename)

    def delete_table(self, table, optlist):
        PDF_delete_table(self.p, table, optlist)

    def delete_textflow(self, textflow):
        PDF_delete_textflow(self.p, textflow)

    def draw_path(self, path, x, y, optlist):
        PDF_draw_path(self.p, path, x, y, optlist)

    def ellipse(self, x, y, rx, ry):
        PDF_ellipse(self.p, x, y, rx, ry)

    def elliptical_arc(self, x, y, rx, ry, optlist):
        PDF_elliptical_arc(self.p, x, y, rx, ry, optlist)

    def encoding_set_char(self, encoding, slot, glyphname, uv):
        PDF_encoding_set_char(self.p, encoding, slot, glyphname, uv)

    def end_document(self, optlist):
        PDF_end_document(self.p, optlist)

    def end_dpart(self, optlist):
        PDF_end_dpart(self.p, optlist)

    def end_font(self):
        PDF_end_font(self.p)

    def end_glyph(self):
        PDF_end_glyph(self.p)

    def end_item(self, id):
        PDF_end_item(self.p, id)

    def end_layer(self):
        PDF_end_layer(self.p)

    def end_mc(self):
        PDF_end_mc(self.p)

    def end_page(self):
        PDF_end_page(self.p)

    def end_page_ext(self, optlist):
        PDF_end_page_ext(self.p, optlist)

    def end_pattern(self):
        PDF_end_pattern(self.p)

    def end_template(self):
        PDF_end_template(self.p)

    def end_template_ext(self, width, height):
        PDF_end_template_ext(self.p, width, height)

    def endpath(self):
        PDF_endpath(self.p)

    def fill(self):
        PDF_fill(self.p)

    def fill_graphicsblock(self, page, blockname, graphics, optlist):
        return PDF_fill_graphicsblock(self.p, page, blockname, graphics, optlist)

    def fill_imageblock(self, page, blockname, image, optlist):
        return PDF_fill_imageblock(self.p, page, blockname, image, optlist)

    def fill_pdfblock(self, page, blockname, contents, optlist):
        return PDF_fill_pdfblock(self.p, page, blockname, contents, optlist)

    def fill_stroke(self):
        PDF_fill_stroke(self.p)

    def fill_textblock(self, page, blockname, text, optlist):
        return PDF_fill_textblock(self.p, page, blockname, text, optlist)

    def findfont(self, fontname, encoding, embed):
        return PDF_findfont(self.p, fontname, encoding, embed)

    def fit_graphics(self, graphics, x, y, optlist):
        PDF_fit_graphics(self.p, graphics, x, y, optlist)

    def fit_image(self, image, x, y, optlist):
        PDF_fit_image(self.p, image, x, y, optlist)

    def fit_pdi_page(self, page, x, y, optlist):
        PDF_fit_pdi_page(self.p, page, x, y, optlist)

    def fit_table(self, table, llx, lly, urx, ury, optlist):
        return PDF_fit_table(self.p, table, llx, lly, urx, ury, optlist)

    def fit_textflow(self, textflow, llx, lly, urx, ury, optlist):
        return PDF_fit_textflow(self.p, textflow, llx, lly, urx, ury, optlist)

    def fit_textline(self, text, x, y, optlist):
        PDF_fit_textline(self.p, text, x, y, optlist)

    def get_apiname(self):
        return PDF_get_apiname(self.p)

    def get_buffer(self):
        return PDF_get_buffer(self.p)

    def get_errmsg(self):
        return PDF_get_errmsg(self.p)

    def get_errnum(self):
        return PDF_get_errnum(self.p)

    def get_option(self, keyword, optlist):
        return PDF_get_option(self.p, keyword, optlist)

    def get_parameter(self, key, modifier):
        return PDF_get_parameter(self.p, key, modifier)

    def get_pdi_parameter(self, key, doc, page, reserved):
        return PDF_get_pdi_parameter(self.p, key, doc, page, reserved)

    def get_pdi_value(self, key, doc, page, reserved):
        return PDF_get_pdi_value(self.p, key, doc, page, reserved)

    def get_string(self, idx, optlist):
        return PDF_get_string(self.p, idx, optlist)

    def get_value(self, key, modifier):
        return PDF_get_value(self.p, key, modifier)

    def info_font(self, font, keyword, optlist):
        return PDF_info_font(self.p, font, keyword, optlist)

    def info_graphics(self, graphics, keyword, optlist):
        return PDF_info_graphics(self.p, graphics, keyword, optlist)

    def info_image(self, image, keyword, optlist):
        return PDF_info_image(self.p, image, keyword, optlist)

    def info_matchbox(self, boxname, num, keyword):
        return PDF_info_matchbox(self.p, boxname, num, keyword)

    def info_path(self, path, keyword, optlist):
        return PDF_info_path(self.p, path, keyword, optlist)

    def info_pdi_page(self, page, keyword, optlist):
        return PDF_info_pdi_page(self.p, page, keyword, optlist)

    def info_pvf(self, filename, keyword):
        return PDF_info_pvf(self.p, filename, keyword)

    def info_table(self, table, keyword):
        return PDF_info_table(self.p, table, keyword)

    def info_textflow(self, textflow, keyword):
        return PDF_info_textflow(self.p, textflow, keyword)

    def info_textline(self, text, keyword, optlist):
        return PDF_info_textline(self.p, text, keyword, optlist)

    def initgraphics(self):
        PDF_initgraphics(self.p)

    def lineto(self, x, y):
        PDF_lineto(self.p, x, y)

    def load_3ddata(self, filename, optlist):
        return PDF_load_3ddata(self.p, filename, optlist)

    def load_asset(self, type, filename, optlist):
        return PDF_load_asset(self.p, type, filename, optlist)

    def load_font(self, fontname, encoding, optlist):
        return PDF_load_font(self.p, fontname, encoding, optlist)

    def load_graphics(self, type, filename, optlist):
        return PDF_load_graphics(self.p, type, filename, optlist)

    def load_iccprofile(self, profilename, optlist):
        return PDF_load_iccprofile(self.p, profilename, optlist)

    def load_image(self, imagetype, filename, optlist):
        return PDF_load_image(self.p, imagetype, filename, optlist)

    def makespotcolor(self, spotname):
        return PDF_makespotcolor(self.p, spotname)

    def mc_point(self, tagname, optlist):
        PDF_mc_point(self.p, tagname, optlist)

    def moveto(self, x, y):
        PDF_moveto(self.p, x, y)

    def open_CCITT(self, filename, width, height, BitReverse, K, BlackIs1):
        return PDF_open_CCITT(self.p, filename, width, height, BitReverse, K, BlackIs1)

    def open_file(self, filename):
        return PDF_open_file(self.p, filename)

    def open_image(self, imagetype, source, data, width, height, components, bpc, params):
        return PDF_open_image(self.p, imagetype, source, data, width, height, components, bpc, params)

    def open_image_file(self, imagetype, filename, stringparam, intparam):
        return PDF_open_image_file(self.p, imagetype, filename, stringparam, intparam)

    def open_pdi(self, filename, filename_len):
        return PDF_open_pdi(self.p, filename, filename_len)

    def open_pdi_document(self, filename, optlist):
        return PDF_open_pdi_document(self.p, filename, optlist)

    def open_pdi_page(self, doc, pagenumber, optlist):
        return PDF_open_pdi_page(self.p, doc, pagenumber, optlist)

    def pcos_get_number(self, doc, path):
        return PDF_pcos_get_number(self.p, doc, path)

    def pcos_get_string(self, doc, path):
        return PDF_pcos_get_string(self.p, doc, path)

    def pcos_get_stream(self, doc, optlist, path):
        return PDF_pcos_get_stream(self.p, doc, optlist, path)

    def place_image(self, image, x, y, scale):
        PDF_place_image(self.p, image, x, y, scale)

    def place_pdi_page(self, page, x, y, sx, sy):
        PDF_place_pdi_page(self.p, page, x, y, sx, sy)

    def poca_delete(self, container, optlist):
        PDF_poca_delete(self.p, container, optlist)

    def poca_insert(self, container, optlist):
        PDF_poca_insert(self.p, container, optlist)

    def poca_new(self, optlist):
        return PDF_poca_new(self.p, optlist)

    def poca_remove(self, container, optlist):
        PDF_poca_remove(self.p, container, optlist)

    def process_pdi(self, doc, page, optlist):
        return PDF_process_pdi(self.p, doc, page, optlist)

    def rect(self, x, y, width, height):
        PDF_rect(self.p, x, y, width, height)

    def restore(self):
        PDF_restore(self.p)

    def resume_page(self, optlist):
        PDF_resume_page(self.p, optlist)

    def rotate(self, phi):
        PDF_rotate(self.p, phi)

    def save(self):
        PDF_save(self.p)

    def scale(self, sx, sy):
        PDF_scale(self.p, sx, sy)

    def set_border_color(self, red, green, blue):
        PDF_set_border_color(self.p, red, green, blue)

    def set_border_dash(self, b, w):
        PDF_set_border_dash(self.p, b, w)

    def set_border_style(self, style, width):
        PDF_set_border_style(self.p, style, width)

    def set_graphics_option(self, optlist):
        PDF_set_graphics_option(self.p, optlist)

    def set_gstate(self, gstate):
        PDF_set_gstate(self.p, gstate)

    def set_info(self, key, value):
        PDF_set_info(self.p, key, value)

    def set_layer_dependency(self, type, optlist):
        PDF_set_layer_dependency(self.p, type, optlist)

    def set_option(self, optlist):
        PDF_set_option(self.p, optlist)

    def set_parameter(self, key, value):
        PDF_set_parameter(self.p, key, value)

    def set_text_option(self, optlist):
        PDF_set_text_option(self.p, optlist)

    def set_text_pos(self, x, y):
        PDF_set_text_pos(self.p, x, y)

    def set_value(self, key, value):
        PDF_set_value(self.p, key, value)

    def setcolor(self, fstype, colorspace, c1, c2, c3, c4):
        PDF_setcolor(self.p, fstype, colorspace, c1, c2, c3, c4)

    def setdash(self, b, w):
        PDF_setdash(self.p, b, w)

    def setdashpattern(self, optlist):
        PDF_setdashpattern(self.p, optlist)

    def setflat(self, flatness):
        PDF_setflat(self.p, flatness)

    def setfont(self, font, fontsize):
        PDF_setfont(self.p, font, fontsize)

    def setgray(self, gray):
        PDF_setgray(self.p, gray)

    def setgray_fill(self, gray):
        PDF_setgray_fill(self.p, gray)

    def setgray_stroke(self, gray):
        PDF_setgray_stroke(self.p, gray)

    def setlinecap(self, linecap):
        PDF_setlinecap(self.p, linecap)

    def setlinejoin(self, linejoin):
        PDF_setlinejoin(self.p, linejoin)

    def setlinewidth(self, width):
        PDF_setlinewidth(self.p, width)

    def setmatrix(self, a, b, c, d, e, f):
        PDF_setmatrix(self.p, a, b, c, d, e, f)

    def setmiterlimit(self, miter):
        PDF_setmiterlimit(self.p, miter)

    def setpolydash(self, dasharray, length):
        PDF_setpolydash(self.p, dasharray, length)

    def setrgbcolor(self, red, green, blue):
        PDF_setrgbcolor(self.p, red, green, blue)

    def setrgbcolor_fill(self, red, green, blue):
        PDF_setrgbcolor_fill(self.p, red, green, blue)

    def setrgbcolor_stroke(self, red, green, blue):
        PDF_setrgbcolor_stroke(self.p, red, green, blue)

    def shading(self, type, x0, y0, x1, y1, c1, c2, c3, c4, optlist):
        return PDF_shading(self.p, type, x0, y0, x1, y1, c1, c2, c3, c4, optlist)

    def shading_pattern(self, shading, optlist):
        return PDF_shading_pattern(self.p, shading, optlist)

    def shfill(self, shading):
        PDF_shfill(self.p, shading)

    def show(self, text):
        PDF_show(self.p, text)

    def show_boxed(self, text, left, top, width, height, hmode, feature):
        return PDF_show_boxed(self.p, text, left, top, width, height, hmode, feature)

    def show_xy(self, text, x, y):
        PDF_show_xy(self.p, text, x, y)

    def skew(self, alpha, beta):
        PDF_skew(self.p, alpha, beta)

    def stringwidth(self, text, font, fontsize):
        return PDF_stringwidth(self.p, text, font, fontsize)

    def stroke(self):
        PDF_stroke(self.p)

    def suspend_page(self, optlist):
        PDF_suspend_page(self.p, optlist)

    def translate(self, tx, ty):
        PDF_translate(self.p, tx, ty)

