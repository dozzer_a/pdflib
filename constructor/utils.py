from os import remove, path
from sys import exc_info
from traceback import print_tb
from constructor.PDFlib import PDFlib
from pdflib_py import *


def delete_file():
    # Delete previous pdf file
    file_path = "test.pdf"
    if path.isfile(file_path):
        remove(file_path)
    else:
        print(" {} file was not found".format(file_path))


class PDF(object):

    p = PDFlib()

    x = 595
    y = 792

    page_count = 1

    def __init__(self, font=("Helvetica", "winansi", ""), font_size=10.5, text_position=(50, 70), title="Test PDF"):

        # try:
        self.p.set_option("errorpolicy=return")
        if self.p.begin_document("test.pdf", "") == -1:
            print(self.p.get_errmsg())
            raise Exception("Error")
        self.p.set_info("Author", "Developer")
        self.p.set_info("Creator", "Developer")
        self.p.set_info("Title", title)

        self.p.begin_page_ext(595, 842, "")  # A4 size

        font = self.p.load_font(*font)
        if font == -1:
            raise PDFlibException("Error: " + self.p.get_errmsg())

        self.p.setfont(font, font_size)
        self.p.set_text_pos(*text_position)
        # self.p.show("This is a test text to display :)")
        # self.p.continue_text("(says Python)")

    def add_textline(self, text="Head", x=50, y=None, optlist="fontsize=10.5 strokewidth=1.5 textrendering=2"):
        if not y:
            if self.y <= 0:
                self.new_page()
                self.y = 792
            y = self.y
        print(y)
        # text = "1234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345"
        print(self.p.fit_textline(text, x, y, optlist))
        self.y -= 50

    def add_textflow(self, text="Textflow", llx1=50, lly1=50, urx1=545, ury1=800,
                     optlist="verticalalign=justify linespreadlimit=120% "):

        text = '\n\n<alignment=center fontsize=12>WAIVER OF SERVICE\n\n\n\n'\
        '<alignment=left fillcolor=darkblue fontsize=10>Acacia Rae Moore<fillcolor=black> appeared in person before me today and stated under oath:\n\n'\
        '"I <fillcolor=darkblue>Acacia Rae Moore<fillcolor=black> am the person named as Respondent in this case.\n\n'\
        '"I acknowledge that I have been provided a copy of the Original Petition for Divorce filed in this case. '\
        'I have read and understand the contents of that document.\n\n'\
        '"I understand that the Texas Rules of Civil Procedure require, in most instances, that a party or '\
        'respondentbe served with citation. I do not want to be served with citation, and I waive the issuance and '\
        'service of citation.\n\n'\
        '"I enter my appearance in this case for all purposes.\n\n'\
        '"I waive the making of a record of testimony in this case.\n\n'\
        '"I agree that this case may be taken up and considered by the Court without further notice to me.\n\n'\
        '"I agree that the case may be decided by the presiding Judge of the Court or by a duly appointed '\
        'Associate Judge of the Court.\n\n'\
        '"I further state that the following information is correct and that my\n\n'\
        'Mailing address is: <fillcolor=darkblue>10042 Spring Place Dr. Houston, TX  77070 United States<fillcolor=black>\n\n'\
        'Telephone number is: <fillcolor=darkblue>8327075802<fillcolor=black>\n\n'\
        'Social Security Number is: <fillcolor=darkblue>490040129<fillcolor=black>\n\n'\
        'Texas Driver\'s License Number is: <fillcolor=darkblue>37470081<fillcolor=black>\n\n'\
        '"I further understand that I have a duty to notify the Court if my mailing address changes during this '\
        'proceeding."\n'

        # tf = -1

        optlist1 = "fontname=Helvetica fontsize=10.5 encoding=unicode " \
                   "alignment=justify"

        # optlist1 = "encoding=unicode embedding"

        tf = self.p.create_textflow(text, optlist1)
        if tf == -1:
            raise PDFlibException("Error: " + self.p.get_errmsg())

        result = "_boxfull"
        while result == "_boxfull" or result == "_nextpage":
            # Add "showborder to visualize the fitbox borders
            # optlist = "verticalalign=justify linespreadlimit=120% "

            # p.begin_page_ext(0, 0, "width=a4.width height=a4.height")
            # self.new_page()
            # Fill the first column
            result = self.p.fit_textflow(tf, llx1, lly1, urx1, ury1, optlist)
            # result = p.fit_textflow(tf, llx1, lly1, urx1, ury1, optlist)

            # Fill the second column if we have more text*/
            # if (result != "_stop"):
            #     result = p.fit_textflow(tf, llx2, lly2, urx2, ury2, optlist)

            # p.end_page_ext("")

        print(self.p.info_textflow(tf, 'textendy'))
        self.p.delete_textflow(tf)
        # self.p.continue_text('end')

    def print_column(self, text=['first column', 'second_column'], dist_btw_column=50,
                     optlist1="fontname=Helvetica fontsize=10.5 encoding=unicode fillcolor={gray 0} alignment=justify"):
        optlist = "verticalalign=justify linespreadlimit=120% "
        for number in range(len(text)):
            tf = self.p.create_textflow(text[number], optlist1)
            if tf == -1:
                raise PDFlibException("Error: " + self.p.get_errmsg())

            # x1 = 50 + ((495-dist_btw_column*(len(text)-1)) * number / len(text)) + dist_btw_column*number
            # x2 = 50 + ((495-dist_btw_column*(len(text)-1)) * (number + 1) / len(text)) + dist_btw_column*number

            x1 = 50 + ((495 + dist_btw_column) * number) / len(text)
            x2 = 50 + (495 + dist_btw_column) * (number + 1) / len(text) - dist_btw_column
            result = self.p.fit_textflow(tf, x1, 50, x2, 792, optlist)
            print(545*number/len(text))
            self.p.delete_textflow(tf)

    def add_signature_place(self, width=200):
        optlist = "boxsize={200 0} leader={alignment={none right} text={_}}"
        x = self.x - width - 50
        self.p.fit_textline(' ', self.x-width-50, self.y, optlist)
        print(self.p.info_textline('adc', 'xheight', optlist))
        self.p.set_text_pos(x, self.y)
        self.p.continue_text('pls')
        self.p.continue_text('pls')
        print(self.y)

    def new_page(self, font=("Helvetica", "winansi", ""), font_size=10.5):
        self.p.end_page_ext("")
        self.p.begin_page_ext(595, 842, "")  # A4 size
        font = self.p.load_font(*font)
        if font == -1:
            raise PDFlibException("Error: " + self.p.get_errmsg())

        self.p.setfont(font, font_size)

    def draw_line(self, x1=50, x2=200, y1=50, y2=200,):

        # Set the line width
        self.p.setlinewidth(0.5)

        # Set the fill and stroke color
        self.p.setcolor("fillstroke", "gray", 0.0, 0.0, 0.0, 0.0)

        # Set the current point for graphics output
        self.p.moveto(x1, y1)

        # Draw a line from the current point to the supplied point
        self.p.lineto(x2, y2)

        # Stroke the path using the current line width and stroke color, and
        # clear it

        self.p.stroke()

    def end_document(self):
        self.p.end_page_ext("")
        self.p.end_document("")
        self.p.delete()


delete_file()
try:
    pdf = PDF()
    for num in range(1):
        pdf.add_textline(x=(595/2-50))
    print()
    pdf.print_column(text=[
        'IN THE MATTER OF\n'
        'THE MARRIAGE OF\n\n'
        '<fillcolor=darkblue>Darryl Andrew Bradley<fillcolor=black>\n\n'
        'AND\n\n'
        '<fillcolor=darkblue>Acacia Rae Moore<fillcolor=black>\n\n'
        'AND IN THE INTERESTS OF\n\n'
        '<fillcolor=darkblue>De\'mari Emmanuel Moore- Bradley<fillcolor=black>\n\n\n\n'
        'MINOR CHILDREN',

        '<alignment=center>IN THE DISTRICT COURT\n\n\n\n\n\n\n'
        '11th JUDICIAL DISTRICTAND\n\n\n\n\n\n\n\n'
        '<fillcolor=darkblue>HARRIS<fillcolor=black> COUNTY, TEXAS'
    ])
    pdf.add_textflow()
    # pdf.draw_line()
    # pdf.add_signature_place()
    pdf.end_document()
except PDFlibException:
    print("PDFlib exception occurred:\n[{}] {}: {}".format(pdf.p.get_errnum(),
                                                           pdf.p.get_apiname(),
                                                           pdf.p.get_errmsg()))
    print_tb(exc_info()[2])

except Exception:
    print("Exception occurred: %s" % (exc_info()[0]))
    print_tb(exc_info()[2])
