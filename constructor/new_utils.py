from os import remove, path
from sys import exc_info
from traceback import print_tb
from constructor.PDFlib import PDFlib
from pdflib_py import *


def delete_file():
    # Delete previous pdf file
    file_path = "test.pdf"
    if path.isfile(file_path):
        remove(file_path)
    else:
        print(" {} file was not found".format(file_path))


class PDF(object):

    p = PDFlib()

    a4_height = 842
    a4_width = 595

    indent_x = 50
    indent_y = 50

    x = a4_width - indent_x
    y = a4_height - indent_y

    font = ("Helvetica", "winansi", "")
    font_size = 12

    page_count = 1

    def __init__(self, font=("Helvetica", "winansi", ""), font_size=10.5, title="Test PDF",
                 indent_x=50, indent_y=50):

        self.p.set_option("errorpolicy=return")
        if self.p.begin_document("test.pdf", "") == -1:
            print(self.p.get_errmsg())
            raise Exception("Error")
        self.p.set_info("Author", "Developer")
        self.p.set_info("Creator", "Developer")
        self.p.set_info("Title", title)

        self.indent_x = indent_x
        self.indent_y = indent_y

        self.x = self.a4_width - indent_x
        self.y = self.a4_height - indent_y

        self.p.begin_page_ext(0, 0, "width=a4.width height=a4.height")  # A4 size

        self.font = font
        self.font_size = font_size

        font = self.p.load_font(*font)
        if font == -1:
            raise PDFlibException("Error: " + self.p.get_errmsg())

        self.p.setfont(font, font_size)
        self.p.set_text_pos(self.indent_x, self.a4_height-self.indent_y)

    def new_page(self, font=font, font_size=font_size):
        self.p.end_page_ext("")
        self.p.begin_page_ext(0, 0, "width=a4.width height=a4.height")  # A4 size

        font = self.p.load_font(*font)
        if font == -1:
            raise PDFlibException("Error: " + self.p.get_errmsg())

        self.p.setfont(font, font_size)
        self.p.set_text_pos(self.indent_x, self.a4_height-self.indent_y)

        self.x = self.a4_width - self.indent_x
        self.y = self.a4_height - self.indent_y

        self.page_count += 1
        self.p.fit_textline("Page {}".format(self.page_count), 298, 10, "fontsize=8")

    def print_text(self, text="", text_optlist="fontname=Helvetica encoding=unicode alignment=justify"):
        text_optlist += " fontsize={}".format(self.font_size)

        optlist = "verticalalign=justify linespreadlimit=120%"

        tf = self.p.create_textflow(text, text_optlist)
        if tf == -1:
            raise PDFlibException("Error: " + self.p.get_errmsg())

        result = "_boxfull"
        while result == "_boxfull":
            result = self.p.fit_textflow(tf, self.indent_x, self.indent_y, self.a4_width-self.indent_x, self.y, optlist)
            if result == "_boxfull":
                self.new_page()

        # Check for errors
        if result != "_stop":
            # "_boxempty" happens if the box is very small and doesn't
            # hold any text at all.

            if result == "_boxempty":
                raise PDFlibException("Error: Textflow box too small")
            else:
                # Any other return value is a user exit caused by
                # the "return" option; this requires dedicated code to
                # deal with.
                raise PDFlibException("User return '" + result + "' found in Textflow")

        self.y = self.p.info_textflow(tf, 'textendy')
        self.p.delete_textflow(tf)
        # self.p.continue_text('end')

    def print_column(self, text=['first column', 'second column'], dist_btw_column=50,
                     optlist1="fontname=Helvetica encoding=unicode fillcolor={gray 0} alignment=justify"):
        optlist1 += " fontsize={}".format(self.font_size)
        optlist = "verticalalign=justify linespreadlimit=120% "
        for col_number in range(len(text)):
            tf = self.p.create_textflow(text[col_number], optlist1)
            if tf == -1:
                raise PDFlibException("Error: " + self.p.get_errmsg())

            # x1 = 50 + ((495-dist_btw_column*(len(text)-1)) * number / len(text)) + dist_btw_column*number
            # x2 = 50 + ((495-dist_btw_column*(len(text)-1)) * (number + 1) / len(text)) + dist_btw_column*number

            x1 = self.indent_x + ((self.a4_width - 2*self.indent_x + dist_btw_column) * col_number) / len(text)
            x2 = self.indent_x + (self.a4_width - 2*self.indent_x + dist_btw_column) * (col_number + 1) / len(text) - dist_btw_column

            result = "_boxfull"
            while result == "_boxfull":
                result = self.p.fit_textflow(tf, x1, self.indent_y, x2, self.y, optlist)
                if result == "_boxfull":
                    self.new_page()

            # Check for errors
            if result != "_stop":
                # "_boxempty" happens if the box is very small and doesn't
                # hold any text at all.

                if (result == "_boxempty"):
                    raise PDFlibException("Error: Textflow box too small")
                else:
                    # Any other return value is a user exit caused by
                    # the "return" option; this requires dedicated code to
                    # deal with.
                    raise PDFlibException("User return '" + result + "' found in Textflow")

            y = self.p.info_textflow(tf, 'textendy')
            self.p.delete_textflow(tf)
        self.y = y

    def print_signature_above(self, text='', x=295, optlist1="fontname=Helvetica encoding=unicode"):
        optlist1 += " fontsize={}".format(self.font_size)
        text = '\n\n\n<overline leader={alignment={none right} text={ }}>' + text

        optlist = "verticalalign=justify linespreadlimit=120% "
        tf = self.p.create_textflow(text, optlist1)
        if tf == -1:
            raise PDFlibException("Error: " + self.p.get_errmsg())

        result = "_boxfull"
        while result == "_boxfull":
            result = self.p.fit_textflow(tf, x, self.indent_y, self.x, self.y, optlist)
            if result == "_boxfull":
                self.new_page()

        # Check for errors
        if result != "_stop":
            # "_boxempty" happens if the box is very small and doesn't
            # hold any text at all.

            if result == "_boxempty":
                raise PDFlibException("Error: Textflow box too small")
            else:
                # Any other return value is a user exit caused by
                # the "return" option; this requires dedicated code to
                # deal with.
                raise PDFlibException("User return '" + result + "' found in Textflow")
        self.y = self.p.info_textflow(tf, 'textendy')
        self.p.delete_textflow(tf)

    def print_signature_right(self, text='\n\nSIGNED UNDER oath before me on',
                              optlist1="fontname=Helvetica encoding=unicode"):
        optlist1 += " fontsize={}".format(self.font_size)
        text = text + '<leader={alignment={none right} text={_}}>'
        # optlist = " leader={alignment={none right} text={_}}"
        optlist = "verticalalign=justify linespreadlimit=120% "
        tf = self.p.create_textflow(text, optlist1)
        if tf == -1:
            raise PDFlibException("Error: " + self.p.get_errmsg())

        result = "_boxfull"
        while result == "_boxfull":
            result = self.p.fit_textflow(tf, self.indent_x, self.indent_y, self.x, self.y, optlist)
            if result == "_boxfull":
                self.new_page()

        # Check for errors
        if result != "_stop":
            # "_boxempty" happens if the box is very small and doesn't
            # hold any text at all.

            if result == "_boxempty":
                raise PDFlibException("Error: Textflow box too small")
            else:
                # Any other return value is a user exit caused by
                # the "return" option; this requires dedicated code to
                # deal with.
                raise PDFlibException("User return '" + result + "' found in Textflow")
        self.y = self.p.info_textflow(tf, 'textendy')
        self.p.delete_textflow(tf)

        # self.p.fit_textline(text, self.indent_x, self.y, optlist)
        return

    def print_link(self, link):
        return

    def draw_line(self, x1=50, x2=200, y1=50, y2=200,):

        # Set the line width
        self.p.setlinewidth(0.5)

        # Set the fill and stroke color
        self.p.setcolor("fillstroke", "gray", 0.0, 0.0, 0.0, 0.0)

        # Set the current point for graphics output
        self.p.moveto(x1, y1)

        # Draw a line from the current point to the supplied point
        self.p.lineto(x2, y2)

        # Stroke the path using the current line width and stroke color, and
        # clear it

        self.p.stroke()

    def end_document(self):
        self.p.end_page_ext("")
        self.p.end_document("")
        # self.p.delete()


text = '\n\n\n\n<alignment=center fontsize=15>WAIVER OF SERVICE\n\n\n\n'\
'<alignment=left fillcolor=darkblue fontsize=12>Acacia Rae Moore<fillcolor=black> appeared in person before me today and stated under oath:\n\n'\
'"I <fillcolor=darkblue>Acacia Rae Moore<fillcolor=black> am the person named as Respondent in this case.\n\n'\
'"I acknowledge that I have been provided a copy of the Original Petition for Divorce filed in this case. '\
'I have read and understand the contents of that document.\n\n'\
'"I understand that the Texas Rules of Civil Procedure require, in most instances, that a party or '\
'respondentbe served with citation. I do not want to be served with citation, and I waive the issuance and '\
'service of citation.\n\n'\
'"I enter my appearance in this case for all purposes.\n\n'\
'"I waive the making of a record of testimony in this case.\n\n'\
'"I agree that this case may be taken up and considered by the Court without further notice to me.\n\n'\
'"I agree that the case may be decided by the presiding Judge of the Court or by a duly appointed '\
'Associate Judge of the Court.\n\n'\
'"I further state that the following information is correct and that my\n\n'\
'Mailing address is: <fillcolor=darkblue>10042 Spring Place Dr. Houston, TX  77070 United States<fillcolor=black>\n\n'\
'Telephone number is: <fillcolor=darkblue>8327075802<fillcolor=black>\n\n'\
'Social Security Number is: <fillcolor=darkblue>490040129<fillcolor=black>\n\n'\
'Texas Driver\'s License Number is: <fillcolor=darkblue>37470081<fillcolor=black>\n\n'\
'"I further understand that I have a duty to notify the Court if my mailing address changes during this '\
'proceeding."'

delete_file()
try:
    pdf = PDF(font_size=12, indent_y=80)
    pdf.print_column(text=[

        'IN THE MATTER OF\n'
        'THE MARRIAGE OF\n\n'
        '<fillcolor=darkblue>Darryl Andrew Bradley<fillcolor=black>\n\n'
        'AND\n\n'
        '<fillcolor=darkblue>Acacia Rae Moore<fillcolor=black>\n\n'
        'AND IN THE INTERESTS OF\n\n'
        '<fillcolor=darkblue>De\'mari Emmanuel Moore- Bradley<fillcolor=black>\n\n\n\n'
        'MINOR CHILDREN',

        '<alignment=center>IN THE DISTRICT COURT\n\n\n\n\n\n\n'
        '11th JUDICIAL DISTRICTAND\n\n\n\n\n\n\n\n'
        '<fillcolor=darkblue>HARRIS<fillcolor=black> COUNTY, TEXAS'
    ])
    pdf.print_text(text=text)
    pdf.print_signature_above(
        '<fillcolor=darkblue>Acacia Rae Moore<fillcolor=black >\n'
        '<overline=false>Respondent')
    pdf.print_signature_right('\n\nSIGNED UNDER oath before me on',)
    pdf.print_signature_above('Notary Public, State of Texas')
    pdf.print_signature_above()
    pdf.end_document()

except PDFlibException:
    print("PDFlib exception occurred:\n[{}] {}: {}".format(pdf.p.get_errnum(),
                                                           pdf.p.get_apiname(),
                                                           pdf.p.get_errmsg()))
    print_tb(exc_info()[2])

except Exception:
    print("Exception occurred: %s" % (exc_info()[0]))
    print_tb(exc_info()[2])

finally:
    pdf.p.delete()
