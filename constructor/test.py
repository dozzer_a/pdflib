from constructor.PDFlib import PDFlib
from sys import exc_info
from traceback import print_tb
from pdflib_py import PDFlibException
from constructor.new_utils import PDF


try:
    pdf = PDF()
    pdf.add_textline('NO:')
    pdf.print_text(
        'IN THE MATTER OF\n'
        'THE MARRIAGE OF\n\n'
        'Darryl Andrew  Bradley\n\n'
        'AND\n\n'
        'Acacia Rae Moore\n\n'
        'AND IN THE INTERESTS OF\n\n'
        'De\'mari Emmanuel Moore- Bradley\n\n\n'
        'MINOR CHILDREN')

    pdf.print_text(
        'IN THE DISTRICT COURT\n\n\n\n\n'
        '11th JUDICIAL DISTRICTAND\n\n\n\n\n'
        'HARRIS COUNTY, TEXAS'
    )

    pdf.print_text(
        '\n\nWAIVER OF SERVICE\n\n'
        'Acacia Rae Moore appeared in person before me today and stated under oath:\n'
        '"I Acacia Rae Moore, am the person named as Respondent in this case.\n'
        '"I acknowledge that I have been provided a copy of the Original Petition for Divorce filed in this case. '
        'Ihave read and understand the contents of that document.\n'
        '"I understand that the Texas Rules of Civil Procedure require, in most instances, that a party or '
        'respondentbe served with citation. I do not want to be served with citation, and I waive the issuance and '
        'service of citation.\n'
        '"I enter my appearance in this case for all purposes.\n'
        '"I waive the making of a record of testimony in this case.\n'
        '"I agree that this case may be taken up and considered by the Court without further notice to me.\n'
        '"I agree that the case may be decided by the presiding Judge of the Court or by a duly appointed '
        'AssociateJudge of the Court.\n'
        '"I further state that the following information is correct and that my\n'
        'Mailing address is: 10042 Spring Place Dr. Houston, TX  77070 United States Telephone\n'
        'number is: 8327075802Social\n'
        'Security Number is: 490040129\n'
        'Texas Driver\'s License Number is: 37470081\n'
        '"I further understand that I have a duty to notify the Court if my mailing address changes during this '
        'proceeding."\n'
    )

    pdf.add_textline(
        '\nAcacia Rae Moore\n'
        'Respondent')
    pdf.add_textline(
        '\nSIGNED UNDER oath before me on')
    pdf.add_textline(
        '\nNotary Public, State of Texas')

except PDFlibException:
    print("PDFlib exception occurred:\n[{}] {}: {}".format(pdf.p.get_errnum(),
                                                           pdf.p.get_apiname(),
                                                           pdf.p.get_errmsg()))
    print_tb(exc_info()[2])

except Exception:
    print("Exception occurred: %s" % (exc_info()[0]))
    print_tb(exc_info()[2])

finally:
    pdf.p.delete()
